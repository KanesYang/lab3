var mongoose     = require('mongoose');
var Schema       = mongoose.Schema;

mongoose.connect("mongodb://localhost/nodetest2")
var db = mongoose.connection;

var UserSchema   = new Schema({
    username: String,
    age: Number,
    fullname: String,
    email: String,
    sex: String,
    photo: String
});

var User = module.exports = mongoose.model('User', UserSchema);

module.exports = mongoose.model('User', UserSchema);
