var express = require('express');
var fs = require('fs');
var mongoose = require('mongoose');
var router = express.Router();
var multer = require('multer');
var upload = multer({
    dest: './public/images'
});

var User = require("../model/user");

/* GET userlist. */
router.get('/userlist', function (req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    collection.find({}, {}, function (e, docs) {
        res.json(docs);
    });
});

router.post('/adduser', upload.single('profileimage'), function (req, res) {
    var db = req.db;
    var collection = db.get('userlist');

    var username = req.body.username;
    var email = req.body.email;
    var age = req.body.age;
    var sex = req.body.sex;
    var fullname = req.body.fullname;
    var profileimage = req.body.profileimage;

//    if (req.file) {
//        var profileimage = req.file.filename;
//    } else {
//        var profileimage = 'noimage.jpg';
//    }

    collection.insert({
        "username": username,
        "email": email,
        "age": age,
        "sex": sex,
        "fullname": fullname,
        "profileimage": profileimage
    }, function (err, result) {
        res.send(
            (err === null) ? {
                msg: ''
            } : {
                msg: err
            }
        );
    });
});

router.put('/updateuser', upload.single('profileimage'), function (req, res) {
    var db = req.db;
    var collection = db.get('userlist');

    var username = req.body.username;
    var email = req.body.email;
    var age = req.body.age;
    var sex = req.body.sex;
    var fullname = req.body.fullname;

    if (req.file) {
        var profileimage = req.file.filename;
    } else {
        var profileimage = 'noimage.jpg';
    }

    collection.update({
        username: req.body.username
    }, {
        "username": username,
        "email": email,
        "age": age,
        "sex": sex,
        "fullname": fullname,
        "profileimage": profileimage
    }, function (err, result) {
        res.send(
            (err === null) ? {
                msg: ''
            } : {
                msg: err
            }
        );
    });
});

router.delete('/deleteuser/:id', function (req, res) {
    var db = req.db;
    var collection = db.get('userlist');
    var userToDelete = req.params.id;

    collection.remove({
        '_id': userToDelete
    }, function (err) {
        res.send((err === null) ? {
            msg: ''
        } : {
            msg: 'error: ' + err
        });
    });
});

module.exports = router;