// Userlist data array for fillling in info box
var userListData = [];

// DOM Ready
$(document).ready(function () {
    // Populate the user table on initial page load
    populateTable();

    // Username link click
    $('#userList table tbody').on('click', 'td a.linkshowuser', showUserInfo);

    // Add User button click
    $('#btnAddUser').on('click', addUser);

    // Add Update button click
    $('#btnUpdateUser').on('click', updateUser);

    // Delete user link click
    $('#userList table tbody').on('click', 'td a.linkdeleteuser', deleteUser);

    // Update user link click
    $('#userList table tbody').on('click', 'td a.linkupdateuser', showUserInfoForUpdation);
});

// Functions

// Fill table with data
function populateTable() {

    // Empty content string
    var tableContent = '';

    // jQuery AJAX call for JSON
    $.getJSON('/users/userlist', function (data) {
        // Stick our user data array into a userlist variable in the global object
        userListData = data;

        // For each item in our JSON, add a table row and cells to the content string
        $.each(data, function () {
            tableContent += '<tr>';
            tableContent += '<td><a href="#" class="linkshowuser" rel="' + this.username + '">' + this.username + '</a></td>';
            tableContent += '<td>' + this.email + '</td>';
            tableContent += '<td>' + this.fullname + '</td>';
            tableContent += '<td>' + this.age + '</td>';
            tableContent += '<td><a href="#" class="linkdeleteuser" rel="' + this._id + '">delete</a></td>';
            tableContent += '<td><a href="#" class="linkupdateuser" rel="' + this._id + '">update</a></td>';
            tableContent += '</tr>';
        });

        // Inject the whole content string into our existing html table
        $('#userList table tbody').html(tableContent);
    });
};

function showUserInfo(event) {
    // Prevent link from firing
    event.preventDefault();

    // Retrieve username from link rel attribute
    var thisUserName = $(this).attr('rel');

    // Get index of object based on id value
    var arrayPosition = userListData.map(function (arrayItem) {
        return arrayItem.username;
    }).indexOf(thisUserName);

    // Get our User object
    var thisUserObject = userListData[arrayPosition];

    console.log(thisUserObject.sex);
    // Populate Info Box
    $('#userInfoName').text(thisUserObject.fullname);
    $('#userInfoAge').text(thisUserObject.age);
    $('#userInfoGender').text(thisUserObject.sex);
    $('#userInfoPhoto').html(function () {
        return '<img src="./images/' + thisUserObject.profileimage + '" alt = "userphoto" style="width:250px; height:250px"/>'
    });
};

function addUser(event) {
    event.preventDefault();

    var oFReader = new FileReader();
    var oFile = document.getElementById('uploadImage').files[0];
    oFReader.readAsDataURL(oFile);
    var imagesrc = oFReader.result;

    //console.log(oFile);

    // Basic validation - increase error count if any fields are blank
    var errorCount = 0;
    $('#addUser input[placeholder]').each(function (index, val) {
        if ($(this).val() === '') {
            errorCount++;
        }
    });

    // Check and make sure errorCount's still at zero
    if (errorCount === 0) {
        // If it is, compile all user info into one oebject
        /*        var newUser = {
                    'username': $('#addUser fieldset input#inputUserName').val(),
                    'email': $('#addUser fieldset input#inputUserEmail').val(),
                    'fullname': $('#addUser fieldset input#inputUserFullname').val(),
                    'age': $('#addUser fieldset input#inputUserAge').val(),
                    'sex': $('input:radio[name="sex"]:checked').val(),
                    'photo': $('#addUser fieldset input#inputUserPhoto').val(),
                    'profileimage': imagesrc
                }*/
        var newUser = new FormData();
        newUser.append('username', $('#addUser fieldset input#inputUserName').val());
        newUser.append('email', $('#addUser fieldset input#inputUserEmail').val());
        newUser.append('fullname', $('#addUser fieldset input#inputUserFullname').val());
        newUser.append('age', $('#addUser fieldset input#inputUserAge').val());
        newUser.append('sex', $('input:radio[name="sex"]:checked').val());
        newUser.append('profileimage', $('#uploadImage')[0].files[0]);

        console.log($('input:radio[name="sex"]:checked').val());
        //console.log(newUser);
        // use AJAX to post object to adduser service
        $.ajax({
            type: 'POST',
            data: newUser,
            url: '/users/adduser',
            dataType: 'JSON',
            contentType: false,
            processData: false
        }).done(function (response) {
            // check for successful response
            if (response.msg === '') {
                // clear form inputs
                $('#addUser fieldset input').val('');

                // Update table
                populateTable();
            } else {
                // if something goes wrong, alert error emssage
                alert('Error: ' + response.msg);
            }
        });
    } else {
        // If error count is > 0, error out
        alert('Please fill in all fields');
        return false;
    }
};

function deleteUser(event) {
    event.preventDefault();

    // Pop up confirmation dialog
    var confirmation = confirm('Are you sure you want to delete this user?');

    // Check and make sure the user confirmed
    if (confirmation === true) {
        // If they did, delete
        $.ajax({
            type: 'DELETE',
            url: '/users/deleteuser/' + $(this).attr('rel')
        }).done(function (response) {
            populateTable();
            // check for success response
            if (response.msg === '') {
                // update table
                populateTable();
            } else {
                alert('Error: ' + response.msg);
            }

        });
    } else {
        // If they said no to the confirmation dialog, do nothing
        return false;
    }
};

function updateUser(event) {
    // Prevent link from firing
    event.preventDefault();

    // Basic validation - increase error count if any fields are blank
    var errorCount = 0;
    $('#addUser input[placeholder]').each(function (index, val) {
        console.log(errorCount);
        if ($(this).val() === '') {
            console.log($(this));
            errorCount++;
        }
    });

    // Check and make sure errorCount's still at zero
    if (errorCount === 0) {
        // If it is, compile all user info into one oebject
        /*        var updateUser = {
                    'username': $('#addUser fieldset input#inputUserName').val(),
                    'email': $('#addUser fieldset input#inputUserEmail').val(),
                    'fullname': $('#addUser fieldset input#inputUserFullname').val(),
                    'age': $('#addUser fieldset input#inputUserAge').val(),
                    'sex': $('input:radio[name="sex"]:checked').val(),
                    //  'gender': $('#addUser fieldset input#inputUserGender').val(),
                    'photo': $('#addUser fieldset input#inputUserPhoto').val(),
                    'profileimage': $('input[name="profileimage"]').get(0).files[0]
                }*/

        var updateUser = new FormData();
        updateUser.append('username', $('#addUser fieldset input#inputUserName').val());
        updateUser.append('email', $('#addUser fieldset input#inputUserEmail').val());
        updateUser.append('fullname', $('#addUser fieldset input#inputUserFullname').val());
        updateUser.append('age', $('#addUser fieldset input#inputUserAge').val());
        updateUser.append('sex', $('input:radio[name="sex"]:checked').val());
        updateUser.append('profileimage', $('#uploadImage')[0].files[0]);

         console.log($('input:radio[name="sex"]:checked').val());
        // use AJAX to post object to adduser service
        $.ajax({
            type: 'PUT',
            data: updateUser,
            url: '/users/updateuser',
            dataType: 'JSON',
            contentType: false,
            processData: false
        }).done(function (response) {
            // check for successful response
            if (response.msg === '') {
                // clear form inputs
                $('#addUser fieldset input').val('');
                // Update table
                populateTable();
                errorCount = 0;
            } else {
                // if something goes wrong, alert error emssage
                alert('Error: ' + response.msg);
            }
        });
    } else {
        // If error count is > 0, error out
        alert('Please fill in all fields');
        return false;
    }
}

function showUserInfoForUpdation(event) {
    // Prevent link from firing
    event.preventDefault();

    // Retrieve username from link rel attribute
    var thisUserId = $(this).attr('rel');

    // Get index of object based on id value
    var arrayPosition = userListData.map(function (arrayItem) {
        return arrayItem._id;
    }).indexOf(thisUserId);

    // Get our User object
    var thisUserObject = userListData[arrayPosition];

    // Populate Info Box
    $('#addUser fieldset input#inputUserName').val(thisUserObject.username);
    $('#addUser fieldset input#inputUserEmail').val(thisUserObject.email);
    $('#addUser fieldset input#inputUserFullname').val(thisUserObject.fullname);
    $('#addUser fieldset input#inputUserAge').val(thisUserObject.age);
    //  $('#addUser fieldset input#inputUserLocation').val(thisUserObject.location);
    //  $('#addUser fieldset input#inputUserGender').val(thisUserObject.gender);
    //  $('#addUser fieldset input#inputUserPhoto').val(thisUserObject.photo);
};